package net.angle.omninetwork.impl;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.compression.Lz4FrameDecoder;
import io.netty.handler.codec.compression.Lz4FrameEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.angle.omninetwork.api.Datagram;
import net.angle.omninetwork.api.PrioritizedMessageQueue;
import net.angle.omniserialization.api.ObjectSerializerRegistry;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public class BasicNettyClient extends Thread {
    private final ObjectSerializerRegistry serializerRegistry;
    public final PrioritizedMessageQueue inbox = new BasicPrioritizedMessageQueue();
    private final String host;
    private final int port;
    private volatile @Getter boolean running = false;
    private final EventLoopGroup workerGroup = new NioEventLoopGroup();
    private volatile SocketChannel channel = null;
    private final Map<Class<?>, Object> datagramTargets = new HashMap<>();
    
    public BasicNettyClient(ObjectSerializerRegistry serializerRegistry, int port) {
        this(serializerRegistry, "localhost", port);
    }
    
    public BasicNettyClient(ObjectSerializerRegistry serializerRegistry) {
        this(serializerRegistry, "localhost", 25565);
    }
    
    public BasicNettyClient(ObjectSerializerRegistry serializerRegistry, String host) {
        this(serializerRegistry, host, 25565);
    }
    
    public <T> void addDatagramTarget(Class<T> targetClass, T datagramTarget) {
        datagramTargets.put(targetClass, datagramTarget);
    }
    
    public void sendToServer(Datagram datagram) {
        if (!isConnected())
            throw new IllegalStateException("Channel has not yet been initialized!");
        channel.writeAndFlush(datagram);
    }
    
    public void readUpdates() {
        Datagram next;
        while (Objects.nonNull(next = inbox.getNext())) {
            try {
//            System.out.println(next);
                next.execute(datagramTargets.get(next.getTargetClass()));
            } catch(Exception ex) {
                Logger.getLogger(BasicNettyClient.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public boolean isConnected() {
        return channel != null;
    }
    
    public boolean isShuttingDown() {
        return workerGroup.isShuttingDown();
    }
    
    public boolean isShutdown() {
        return workerGroup.isShutdown();
    }
    
    public void init() {
        setName("Client Networking Thread");
        setDaemon(true);
    }
    
    public void initializeConnection() {
    }
    
    @Override
    public void run() {
        try {
            Bootstrap b = new Bootstrap();
            b.group(workerGroup);
            b.channel(NioSocketChannel.class);
            b.option(ChannelOption.SO_KEEPALIVE, true);
            b.handler(new ChannelInitializer<SocketChannel>() {
                @Override
                public void initChannel(SocketChannel socketChannel) throws Exception {
                    socketChannel.pipeline().addLast(new Lz4FrameEncoder(), new Lz4FrameDecoder(), new BasicDatagramEncoder(serializerRegistry), 
                            new BasicDatagramDecoder(serializerRegistry),
                            new BasicDatagramHandler(inbox));
                    channel = socketChannel;
                }
            });
            
            // Start the client.
            ChannelFuture f = b.connect(host, port).sync();
            
            initializeConnection();
            
            // Wait until the connection is closed.
            f.channel().closeFuture().sync();
        } catch (InterruptedException ex) {
        } finally {
            destroy();
        }
    }
    
    public void destroy() {
        channel.close();
        channel = null;
        workerGroup.shutdownGracefully();
    }
}