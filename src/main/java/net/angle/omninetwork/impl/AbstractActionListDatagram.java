package net.angle.omninetwork.impl;

import lombok.Getter;
import lombok.ToString;
import net.angle.omninetwork.api.ActionList;
import net.angle.omninetwork.api.DatagramAction;

/**
 *
 * @author angle
 */
@ToString(callSuper = true)
public abstract class AbstractActionListDatagram<T> extends AbstractDatagram<T> implements ActionList<T>{
    private final @Getter DatagramAction<T>[] actions;

    public AbstractActionListDatagram(DatagramAction<T>[] actions, int priority) {
        super(priority);
        this.actions = actions;
    }

    @Override
    public void execute(T target) {
        executeActions(target);
    }
}