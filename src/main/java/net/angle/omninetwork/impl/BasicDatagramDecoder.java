package net.angle.omninetwork.impl;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ReplayingDecoder;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;
import lombok.AllArgsConstructor;
import net.angle.omninetwork.api.Datagram;
import net.angle.omniserialization.api.ObjectSerializerRegistry;

/**
 *
 * @author angle
 */
@AllArgsConstructor
public class BasicDatagramDecoder extends ReplayingDecoder<Void> {
    private final ObjectSerializerRegistry serializerRegistry;
    

    @Override
    protected void decode(ChannelHandlerContext context, ByteBuf in, List<Object> out) throws Exception {
        int datagramSize = in.readInt();
        if (in.readableBytes() >= datagramSize) {
            ByteBuf bytes = in.readBytes(datagramSize);
            ByteBuffer nioBuffer = bytes.nioBuffer(0, datagramSize);
            nioBuffer.order(ByteOrder.LITTLE_ENDIAN);
            //System.out.println("Reading Datagram, length: " + datagramSize + ", bytes: " + bytes.toString() + ", nioBuffer:" + nioBuffer.toString());
            Datagram datagram = serializerRegistry.readObject(nioBuffer);
            //System.out.println(datagram);
            bytes.release();
            out.add(datagram);
        }
    }
}