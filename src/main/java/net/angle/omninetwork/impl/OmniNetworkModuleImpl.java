package net.angle.omninetwork.impl;

import net.angle.omnimodule.impl.BasicOmniModule;
import net.angle.omninetwork.api.DatagramAction;
import net.angle.omninetwork.api.OmniNetworkModule;
import net.angle.omniserialization.api.ObjectSerializerRegistry;

/**
 *
 * @author angle
 */
public class OmniNetworkModuleImpl extends BasicOmniModule implements OmniNetworkModule {

    public OmniNetworkModuleImpl() {
        super(OmniNetworkModule.class);
    }
    
    @Override
    public void prepSerializerRegistry(ObjectSerializerRegistry registry) {
        registry.registerObject(DatagramAction[].class, DatagramAction[]::new);
    }
}