package net.angle.omninetwork.impl;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.compression.Lz4FrameDecoder;
import io.netty.handler.codec.compression.Lz4FrameEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.angle.omninetwork.api.Datagram;
import net.angle.omninetwork.api.PrioritizedMessageQueue;
import net.angle.omniserialization.api.ObjectSerializerRegistry;

/**
 *
 * @author angle
 */
@RequiredArgsConstructor
public class BasicNettyServer extends Thread {
    private final ObjectSerializerRegistry serializerRegistry;
    public final PrioritizedMessageQueue inbox = new BasicPrioritizedMessageQueue();
    //private final ServerThread server;
    private final int port;
    private final EventLoopGroup bossGroup = new NioEventLoopGroup();
    private final EventLoopGroup workerGroup = new NioEventLoopGroup();
    private volatile @Getter boolean running = false;
    private final List<SocketChannel> channels = Collections.synchronizedList(new ArrayList<>());
    private final PrioritizedMessageQueue messageQueue = new BasicPrioritizedMessageQueue();
    private final Map<Class<?>, Object> datagramTargets = new HashMap<>();

    public BasicNettyServer(ObjectSerializerRegistry serializerRegistry) {
        this(serializerRegistry, 25565);
    }
    
    public void init() {
        setName("Server Networking Thread");
        setDaemon(true);
    }
    
    public <T> void addDatagramTarget(Class<T> targetClass, T datagramTarget) {
        datagramTargets.put(targetClass, datagramTarget);
    }
    
    public void destroy() {
        for (SocketChannel channel: channels) {
            channel.close();
        }
        channels.clear();
        workerGroup.shutdownGracefully();
        bossGroup.shutdownGracefully();
        running = false;
    }
    
    public void sendToAll(Datagram datagram) {
        synchronized (channels) {
            for (SocketChannel channel : channels) {
                channel.writeAndFlush(datagram);
            }
        }
    }
    
    public void readUpdates() {
        Datagram next;
        while (Objects.nonNull(next = inbox.getNext())) {
            try {
//            System.out.println(next);
                next.execute(datagramTargets.get(next.getTargetClass()));
            } catch(Exception ex) {
                Logger.getLogger(BasicNettyServer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void playerConnected(ChannelHandlerContext ctx) {
        
    }
    
    public boolean isShuttingDown() {
        return workerGroup.isShuttingDown();
    }
    
    public boolean isShutdown() {
        return workerGroup.isShutdown();
    }
    
    @Override
    public void run() {
        running = true;
        try {
            ServerBootstrap bootstrap = new ServerBootstrap();
            bootstrap.group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    public void initChannel(SocketChannel channel) throws Exception {
                        channel.pipeline().addLast(new Lz4FrameEncoder(), new Lz4FrameDecoder(), new BasicDatagramEncoder(serializerRegistry), 
                                new BasicDatagramDecoder(serializerRegistry),
                                new BasicDatagramHandler(inbox));
                        channels.add(channel);
                        channel.closeFuture().addListener((f) -> {
                            channels.remove(channel);
                        });
                        channel.pipeline().addLast(new ChannelInboundHandlerAdapter() {
                            @Override
                            public void channelActive(ChannelHandlerContext ctx) throws Exception {
                                super.channelActive(ctx);
                                playerConnected(ctx);
                            }
                        });
                    }
                })
                .option(ChannelOption.SO_BACKLOG, 128)
                .childOption(ChannelOption.SO_KEEPALIVE, true);
    
            // Bind and start to accept incoming connections.
            ChannelFuture f = bootstrap.bind(port).sync();
    
            // Wait until the server socket is closed.
            f.channel().closeFuture().sync();
        } catch (InterruptedException ex) {
        } finally {
            destroy();
        }
    }
}