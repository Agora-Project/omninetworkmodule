package net.angle.omninetwork.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import net.angle.omninetwork.api.Datagram;
import net.angle.omninetwork.api.PrioritizedMessageQueue;

/**
 *
 * @author angle
 */
public class BasicPrioritizedMessageQueue implements PrioritizedMessageQueue {
    private final Map<Integer, ArrayBlockingQueue<Datagram>> outBoxes = new HashMap<>();
    private int greatestPriority = 0, lowestPriority = 0;
    
    @Override
    public void addDatagram(Datagram datagram, int priority) {
        if (!outBoxes.containsKey(priority)) {
            outBoxes.put(priority, new ArrayBlockingQueue<>(1000000));
            greatestPriority = Math.max(greatestPriority, priority);
            lowestPriority = Math.min(lowestPriority, priority);
        }
        outBoxes.get(priority).add(datagram);
    }
    
    @Override
    public void addDatagram(Datagram datagram) {
        addDatagram(datagram, datagram.getPriority());
    }
    
    @Override
    public Datagram getNext() {
        for (int i = greatestPriority; i >= lowestPriority; i--) {
            ArrayBlockingQueue<Datagram> outbox = outBoxes.get(i);
            if (outbox != null && !outbox.isEmpty())
                return outbox.poll();
        }
        return null;
    }
}