package net.angle.omninetwork.impl;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.AllArgsConstructor;
import net.angle.omninetwork.api.Datagram;
import net.angle.omniserialization.api.ObjectSerializerRegistry;
import org.lwjgl.system.MemoryStack;

/**
 *
 * @author angle
 */
@AllArgsConstructor
public class BasicDatagramEncoder extends MessageToByteEncoder<Datagram> {
    private final ObjectSerializerRegistry serializerRegistry;
    
    @Override
    protected void encode(ChannelHandlerContext context, Datagram datagram, ByteBuf out) throws Exception {
        try (MemoryStack stack = MemoryStack.stackPush()) {
            int datagramSize = serializerRegistry.sizeOfObject(datagram);
            ByteBuffer buffer = stack.malloc(datagramSize);
            serializerRegistry.writeObject(datagram, buffer);
            buffer.flip();
            out.writeInt(datagramSize);
            out.writeBytes(buffer);
            //System.out.println("Writing Datagram, length: " + datagramSize + ", bytes: " + buffer.toString() + ", out: " + out.writerIndex());
        } catch(Exception ex) {
            Logger.getLogger(BasicDatagramEncoder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}