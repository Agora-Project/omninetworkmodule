package net.angle.omninetwork.impl;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.AllArgsConstructor;
import net.angle.omninetwork.api.Datagram;
import net.angle.omninetwork.api.MessageQueue
;
/**
 *
 * @author angle
 */
@AllArgsConstructor
public class BasicDatagramHandler extends SimpleChannelInboundHandler<Datagram> {
    private final MessageQueue queue;
    
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        Logger.getLogger(BasicDatagramHandler.class.getName()).log(Level.SEVERE, "Connection handler caught exception!", cause);
        ctx.close();
    }
    
    @Override
    protected void channelRead0(ChannelHandlerContext context, Datagram datagram) throws Exception {
        queue.addDatagram(datagram);
    }
}