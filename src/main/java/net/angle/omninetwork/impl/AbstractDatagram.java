package net.angle.omninetwork.impl;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import net.angle.omninetwork.api.Datagram;

/**
 *
 * @author angle
 */
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public abstract class AbstractDatagram<T> implements Datagram<T> {
    private final @Getter int priority;
}