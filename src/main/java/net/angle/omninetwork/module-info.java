
module omni.network {
    requires static lombok;
    requires io.netty.buffer;
    requires io.netty.transport;
    requires io.netty.handler;
    requires omni.module;
    requires omni.serialization;
    requires devil.util;
    requires org.lwjgl;
    requires java.logging;
    exports net.angle.omninetwork.api;
    exports net.angle.omninetwork.impl;
}