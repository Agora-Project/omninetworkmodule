package net.angle.omninetwork.api;

/**
 *
 * @author angle
 */
public interface MessageQueue {
    public void addDatagram(Datagram datagram);
    public Datagram getNext();
}