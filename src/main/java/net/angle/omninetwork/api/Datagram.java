/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package net.angle.omninetwork.api;

/**
 *
 * @author angle
 */
public interface Datagram<T> extends DatagramAction<T> {
    public int getPriority();
    
    public Class<T> getTargetClass();
}