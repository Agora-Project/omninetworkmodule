package net.angle.omninetwork.api;

/**
 *
 * @author angle
 */
public interface DatagramAction<T> {
    public void execute(T target);
}