package net.angle.omninetwork.api;

import net.angle.omnimodule.api.OmniModule;
import net.angle.omniserialization.api.ObjectSerializerRegistry;

/**
 *
 * @author angle
 */
public interface OmniNetworkModule extends OmniModule {
    public void prepSerializerRegistry(ObjectSerializerRegistry registry);
}