package net.angle.omninetwork.api;

/**
 *
 * @author angle
 */
public interface PrioritizedMessageQueue extends MessageQueue {
    public void addDatagram(Datagram datagram, int priority); 
    public default void addDatagram(Datagram datagram) {
        addDatagram(datagram, datagram.getPriority());
    }
}