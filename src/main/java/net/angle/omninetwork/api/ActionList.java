package net.angle.omninetwork.api;

/**
 *
 * @author angle
 */
public interface ActionList<T> {
    public DatagramAction<T>[] getActions();

    public default void executeActions(T target) {
        for (DatagramAction<T> action : getActions())
            action.execute(target);
    }
}
